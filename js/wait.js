/**
 * Created by Jerome on 08-08-16.
 */

var Wait = {};

Wait.create = function(){
    game.add.text(170, 220, 'Waiting for a second player', { font: "30px Arial", fill: "#19de65"});
    game.stage.disableVisibilityChange = true;
    socket.emit('ready');
};


