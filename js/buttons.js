//plugin bootstrap minus and plus
//http://jsfiddle.net/laelitenetwork/puJ6G/
$(function() {
    // When clicked OK in the bet box, send the bet to the server
    $('#submit-bet').click(function(e){
        e.preventDefault();
        sendBet($('#bet-value').val(),false);
    });

    // When clicked OK in the match bet box, notify server
    $('#ok-match').click(function(e){
        e.preventDefault();
        sendBet($('#match-value').html(),true);
    });

    // When clicked No in the match bet box, notify server
    $('#no-match').click(function(e){
        e.preventDefault();
        noMatch();
    });

    // When clicked OK in the validation of outcome box, start a new round
    $('#ok-btn').click(function(e){
        e.preventDefault();
        $('#okay').hide();
        Game.flush();
        cleanDOM();
        socket.emit('restart');
    });

    $('.btn-number').click(function (e) {
        e.preventDefault();

        var type = $(this).attr('data-type');
        var input = $('#bet-value');
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            if (type == 'minus') {

                if ((currentVal-Game.betIncrement) >= Game.minBet) {
                    input.val(currentVal - Game.betIncrement).change();
                }else{
                    $(this).attr('disabled', true);
                }

            } else if (type == 'plus') {
                if ((currentVal+Game.betIncrement) <= Game.money) { // input.attr('max')
                    input.val(currentVal + Game.betIncrement).change();
                }else {
                    $(this).attr('disabled', true);
                }

            }
        } else {
            input.val(0);
        }
    });
    $('.input-number').focusin(function () {
        $(this).data('oldValue', $(this).val());
    });
    $('.input-number').change(function () {

        var valueCurrent = parseInt($(this).val());
        if (valueCurrent >= Game.minBet) {
            $("#min-btn").removeAttr('disabled')
        } else {
            console.log('Sorry, the minimum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        if (valueCurrent <= Game.money) {
            $("#plus-btn").removeAttr('disabled')
        } else {
            console.log('Sorry, the maximum value was reached');
            $(this).val($(this).data('oldValue'));
        }
    });
    $(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});



function resetButtons(){
    $("#plus-btn").removeAttr('disabled');
    $("#min-btn").removeAttr('disabled');
}