/**
 * Created by Jerome on 04-08-16.
 */

socket = io.connect();

// Change game state once two players are available
socket.on('start',function(){
    game.state.start('Game');
});

// Display a message
socket.on('msg',function(data){
    $('#msg').html(data);
});

// Display both player's money
socket.on('init-money',function(money){
    Game.money = money;
    Game.opponentMoney = money;
    Game.updateMoney(Game.selfMoney,money);
    Game.updateMoney(Game.alterMoney,money);
});

// Tells the UI what is the min bet allowed
socket.on('min-bet',function(minBet){
    Game.minBet = minBet;
    $('#bet-value').val(minBet);
});

// Display the bet dialog
socket.on('bet',function(){
    $('#bet').show();
    $('#bet-value').val(Game.minBet);
});

// Display the match bet dialog
socket.on('match-bet',function(data){
   $('#match-bet').show();
    $('#match-value').html(data.bet);
});

function sendBet(bet,match){
    socket.emit('bet',{bet:bet});
    //console.log('Betting '+bet);
    if(match){
        $('#match-bet').hide();
    }else {
        $('#bet').hide();
    }
}

function noMatch(){
    socket.emit('no-match');
    $('#match-bet').hide();
}

// Update amout of money of the pot
socket.on('pot',function(data){
    Game.updateMoney(Game.pot,data.pot);
});

socket.on('show-send',function(data){
    Game.toggleSend(data.flag);
});

// Animate the appearance of a card
socket.on('new-card',function(data){
    //console.log('Dealing a '+data.card.rank+data.card.suit);
    Game.dealCard(data.idx,data.card,false);
});

function dealHiddenCard(idx){
    Game.dealCard(4-idx,new Card(),true); // Reverse index
}

// Animate the appearance of a card
socket.on('new-hidden-card',function(data){
    Game.removeCard(Game.opponentCards[4-data.idx]);
    dealHiddenCard(data.idx);
});

// Deals 5 hidden cards to opponent
socket.on('hidden-cards',function(){
    for(var i = 0; i < 5; i++){
        dealHiddenCard(i);
        //Game.dealCard(4-i,new Card(),true);
    }
});

// Animate the selection of a card by opponent
socket.on('select-card',function(data){
    Game.selectCard(Game.opponentCards[4-data.idx]); // Reverse index, sine it's in mirror position
});

// Toggle send cards button
socket.on('changed',function(){
    Game.toggleSend(false);
});

// Reveal both players hands
socket.on('reveal-card',function(data){
    //console.log('Revealing '+data.card.rank+data.card.suit);
    //console.log('Owner = '+data.pid+', receiver = '+socket.id);
    if(data.pid.substr(2) == socket.id){return;} // socket id's on server begin with /#, but not on client side
    Game.revealCard(4-data.idx,data.card);
});

// Update money counters
socket.on('update-money',function(data){
    if(data.pid.substr(2) == socket.id){
        Game.updateMoney(Game.selfMoney,data.money);
    }else{
        Game.updateMoney(Game.alterMoney,data.money);
    }
});

// Display ok button before flush
socket.on('over',function(){
    $('#okay').show();
});

// Display win message
socket.on('win',function(){
    Game.winScreen();
    cleanDOM();
});

// Display lose message
socket.on('lose',function(){
    Game.loseScreen();
    cleanDOM();
});

// Go back to waiting state
socket.on('end',function(){
    cleanDOM();
    game.state.start('Wait');
});

function cleanDOM(){
    $('#bet').hide();
    $('#msg').html('');
    Game.sendIcon.visible = false;
    resetButtons();
}