/**
 * Created by Jerome on 04-08-16.
 */
/*
 * Author: Jerome Renaux
 * E-mail: jerome.renaux@gmail.com
 */

var Game = {};
Game.betIncrement = 50; // By how mich can a bet be increased/decreased
Game.minBet = 0;
Game.money = 0; // Money of player
Game.opponentMoney = 0;
Game.handCards = []; // Cards of player
Game.opponentCards = []; // Store the cards of the opponent according to index
Game.canChange = false; // Are clicks allowed on cards to select them for a change of cards

Game.preload = function() {
    game.scale.pageAlignHorizontally = true;
    game.load.image('carpet','assets/bg.jpg');
    game.load.image('money','assets/money.png');
    game.load.image('stack','assets/stack.png');
    game.load.spritesheet('cards','assets/cards.png',79,123);
    game.load.bitmapFont('text', 'fonts/videogame.png', 'fonts/videogame.fnt');
    game.load.bitmapFont('cabbage', 'fonts/cabbage.png', 'fonts/cabbage.fnt');

};

Game.create = function() {
    game.add.tileSprite(0, 0, 700, 490, 'carpet');
    game.add.tileSprite(700, 0, 700, 490, 'carpet');
    game.add.tileSprite(10, 420, 76, 67, 'money');
    game.add.tileSprite(614, 9, 76, 67, 'money');
    Game.sendIcon = game.add.tileSprite(615, 410, 76, 67, 'stack');
    Game.sendIcon.visible = false;
    Game.selfMoney = game.add.bitmapText(0, 385, 'cabbage', '0',12);
    Game.alterMoney = game.add.bitmapText(0, 90, 'cabbage', '0',12);
    Game.pot = game.add.bitmapText(0, 240, 'cabbage', '',24);
    Game.selfMoney.center = 50;
    Game.alterMoney.center = 655;
    Game.pot.center = 350;
    Game.alignMoney(Game.selfMoney);
    Game.alignMoney(Game.alterMoney);
    Game.alignMoney(Game.pot);

    Game.cards = game.add.group();
    for (var i = 0; i < 20; i++) {
        Game.cards.add(game.add.sprite(-79, -123, 'cards'));
    }
    Game.cards.setAll('exists', false);

    Game.sendIcon.inputEnabled = true;
    Game.sendIcon.events.onInputUp.add(Game.sendCards, this);

    socket.emit('loaded');
};

Game.alignMoney= function(text){
    text.x = text.center - (text.textWidth * 0.5);
};

Game.updateMoney = function(counter,amount){
    //+ update max val of bet buttons
    counter.text = amount;
    Game.alignMoney(counter);
};

Game.dealCard = function(idx, cardObj, hidden){
    var card = Game.cards.getFirstExists(false);
    card.exists = true;
    card.idx = idx; // Index in the hand, from 0 to 4
    card.object = cardObj; // associate "card" object containing info about the card
    card.hidden = hidden; // hidden means face down, that is, a card of the opponent

    if(!hidden) { // If player card
        Game.handCards[idx] = card;
        card.inputEnabled = true;
        card.events.onInputUp.add(Game.selectCard, this);
        card.frame = Game.computeFrame(cardObj);
        card.y_dest = 350;
    }else{ // If opponent card
        Game.opponentCards[idx] = card;
        card.frame = 52; // card back
        card.y_dest = 17;
    }
    game.add.tween(card).to({x:(100*idx)+110,y:card.y_dest},800,null,false,idx*250).start(); // destination, duration, easing, autostard, delay
};

// Compute the frame of the card based on its rank and suit
Game.computeFrame = function(cardObj){
    var spriteRow = 0;
    var spriteCol = cardObj.rank;
    switch (cardObj.suit) {
        case "C" :
            spriteRow = 0;
            break;
        case "D" :
            spriteRow = 1;
            break;
        case "H" :
            spriteRow = 2;
            break;
        case "S" :
            spriteRow = 3;
            break;
    }
    switch (spriteCol) {
        case "A" :
            spriteCol = 1;
            break;
        case "J" :
            spriteCol = 11;
            break;
        case "Q" :
            spriteCol = 12;
            break;
        case "K" :
            spriteCol = 13;
            break;
    }
    return (spriteRow * 13) + (spriteCol - 1);
};

Game.selectCard = function(_card){
    if(!Game.canChange){return;}
    var offset = (_card.hidden ? -30 : 30); // In which direction must the card move uppon click, depending on whether it's a player or opponent card
    var to_y = (_card.object.selected ? _card.y_dest : _card.y_dest - offset );
    game.add.tween(_card).to({y: to_y}, 200, null, false, 0).start(); // destination, duration, easing, autostard, delay
    _card.object.selected = !_card.object.selected;
    if(!_card.hidden) {
        socket.emit('select-card', {idx: _card.idx});
    }
};

Game.toggleSend = function(flag){
  Game.sendIcon.visible = flag;
    Game.canChange = flag;
};

Game.sendCards = function(){
    var selected = [];
    for(var i = 0; i < Game.handCards.length; i++){
        var card = Game.handCards[i];
        if(card.object.selected == true){
            Game.removeCard(card);
            selected.push(i);
        }
    }
    //console.log('Sending :'+selected);
    socket.emit('change-cards',{idx:selected});
};

Game.removeCard = function(card){
    //console.log('Removing from '+card.x+', '+card.y);
    var tween = game.add.tween(card).to({x:-79,y:-123},800,null,false,0).start(); // destination, duration, easing, autostard, delay
    tween.onComplete.add(function(){
        card.kill();
    },this);
};

Game.revealCard = function(idx,cardObj){
  Game.opponentCards[idx].frame = Game.computeFrame(cardObj);
};

Game.flush = function(){
    Game.opponentCards.forEach(Game.removeCard);
    Game.handCards.forEach(Game.removeCard);
};

Game.winScreen = function(){
    game.add.bitmapText(game.world.centerX-230, game.world.centerY, 'text', 'YOU WIN',64);
};

Game.loseScreen = function(){
    game.add.bitmapText(game.world.centerX-200, game.world.centerY, 'text', 'YOU LOSE',64);
};