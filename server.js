/*
 // sending to sender-client only
 socket.emit('message', "this is a test");

 // sending to all clients, include sender
 io.emit('message', "this is a test");

 // sending to all clients except sender
 socket.broadcast.emit('message', "this is a test");

 // sending to all clients in 'game' room(channel) except sender
 socket.broadcast.to('game').emit('message', 'nice game');

 // sending to all clients in 'game' room(channel), include sender
 io.in('game').emit('message', 'cool game');

 // sending to sender client, only if they are in 'game' room(channel)
 socket.to('game').emit('message', 'enjoy the game');

 // sending to all clients in namespace 'myNamespace', include sender
 io.of('myNamespace').emit('message', 'gg');

 // sending to individual socketid
 socket.broadcast.to(socketid).emit('message', 'for your eyes only');
 */

var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io').listen(server);

var cards = require('./js/cards.js'); // the "./" is important
var Hand = require('pokersolver').Hand;

app.use('/assets',express.static(__dirname + '/assets'));
app.use('/js',express.static(__dirname + '/js'));
app.use('/css',express.static(__dirname + '/css'));
app.use('/fonts',express.static(__dirname + '/fonts'));

app.get('/',function(req,res){
    res.sendFile(__dirname+'/index.html');
});

server.listen(process.env.PORT || 8081,function(){
    console.log('Listening on '+server.address().port);
});

var Player = function(id){
    this.id = id;
    this.money = 0;
    this.hand = null; // array of "card" objects
    this.solvedHand = null; // the "solved hand" object returned by the hand olver
    this.bet = 0;
    this.loaded = false;
    this.hasBet = false;
    this.hasChanged = false; // has the player changed cards from his hand yet?
    this.hasRestart = false; // has the player validated the outcome of the round yet?
};

var connected = [];
var players = {}; // maps socket.id to player
var startMoney = 1000;
var nbDecks = 4; // how many decks to mix
var minBet = 50;
var highestBet = 0; // Keep track of bets in a betting phase, to make sure both bets match
var gamePhase = 0;
/*
* 0: Waiting for connection
* 1: Making first bet
* 2: Changing cards
* 3: Making second bet
* 4: Reveal
* */
var pot = 0;
var deck = null; // the deck of playing cards

io.on('connection',function(socket){
    socket.on('ready',function(){
        var nbPlayers = countPlayers();
        if(nbPlayers > 2){
            socket.emit('msg','No room left for another player, try again later');
            return;
        }
        if(gamePhase > 0){return;}

        connected.push(socket.id);
        players[socket.id] = new Player(socket.id);
        nbPlayers = countPlayers();
        console.log(socket.id+' has joined');
        //console.log(connected);
        //console.log(nbPlayers+' players');

        if (nbPlayers == 2) {
            gamePhase = 1;
            highestBet = 0;
            io.emit('start',startMoney);
        }
    });

    socket.on('loaded',function(){
        if(players[socket.id] === undefined){return;}
        players[socket.id].loaded = true;
        for (var i = 0; i < connected.length; i++) {
            if(!players[connected[i]].loaded){
                return false;
            }
            players[connected[i]].money = startMoney;
        }
        io.emit('init-money',startMoney);
        io.emit('min-bet',minBet);
        initializeBet();
    });

    socket.on('restart',function(){
        players[socket.id].hasRestart = true;
        if(allPlayersHave('hasRestart')){
            gamePhase = 1;
            initializeBet();
            turnFalse('hasRestart');
        }
    });

    function initializeBet(){
        io.emit('bet');
        io.emit('msg','Place your bet');
    }

    socket.on('bet',function(data){
        if(gamePhase != 1 && gamePhase !=3){console.log('Improper game phase: '+gamePhase);return;}
        handleBet(data,socket);
        if(gamePhase == 2){
            dealCards();
        }else if(gamePhase == 4){
            reveal();
        }
    });

    function handleBet(data,socket){
        var bet = parseInt(data.bet);
        if(bet >= minBet) {
            if (bet <= players[socket.id].money) {
                //console.log(socket.id + ' has bet ' + bet);
                players[socket.id].bet = bet;
                players[socket.id].hasBet = true;
                if (bet > highestBet) {
                    highestBet = bet;
                }
            }else{
                socket.emit('msg','You cannot bet more than you have!');
                socket.emit('bet');
                return;
            }
        }else{
            socket.emit('msg','The minimum bet is '+minBet+'!');
            socket.emit('bet');
            return;
        }

        if(allPlayersHave('hasBet')){
            var tmpPot = 0;
            for (var i = 0; i < connected.length; i++) {
                if(players[connected[i]].bet < highestBet) { // If a bet doesn't match, ask to match
                    io.in(connected[i]).emit('match-bet', {bet: highestBet});
                    players[connected[i]].hasBet = false;
                    return;
                }else{
                    tmpPot += players[connected[i]].bet;
                }
            }
            pot += tmpPot;
            for (var i = 0; i < connected.length; i++) {
                players[connected[i]].money -= players[connected[i]].bet;
                //console.log('Money of '+connected[i]+': '+players[connected[i]].money);
                io.emit('update-money',{pid:connected[i],money:players[connected[i]].money});
            }
            io.emit('pot',{pot:pot});
            turnFalse('hasBet');
            highestBet = 0;
            gamePhase++;
        }
    }

    socket.on('no-match',function(){ // If a player refused to match a bet
        turnFalse('hasBet');
        for (var i = 0; i < connected.length; i++) {
            var loserID = socket.id;
            var winnerID = null;
            if(connected[i] != socket.id){
                winnerID = connected[i];
                break;
            }
        }
        wrapUp(winnerID,loserID);
    });

    function dealCards(){
        deck = new cards.Stack();
        deck.makeDeck(nbDecks); // number of decks
        deck.shuffle(1); // number of shuffling

        for (var i = 0; i < connected.length; i++) {
            //console.log('Dealing cards to '+connected[i]);
            players[connected[i]].hand = new cards.Stack();
            for (var j = 0; j < 5; j++) {
                var card = deck.deal();
                players[connected[i]].hand.addCard(card);
                io.in(connected[i]).emit('new-card', {idx: j, card: card}); // all clients join a room corresponding to their socket.id
                //socket.broadcast.to(players[i]).emit('new-card', {idx: j, card: card}); => doesn't work because a broadcast doesn't inclure the sender!!
            }
            io.in(connected[i]).emit('hidden-cards');
        }
        io.emit('msg','Select the cards you want to discard');
        io.emit('show-send',{flag:true});
    }

    socket.on('select-card',function(data){
        if(gamePhase != 2){console.log('Improper game phase: '+gamePhase); return;}
        players[socket.id].hand.cards[data.idx].selected = !players[socket.id].hand.cards[data.idx].selected;
        socket.broadcast.emit('select-card',data);
    });

    socket.on('change-cards',function(data){
        if(gamePhase != 2){console.log('Improper game phase: '+gamePhase); return;}
        for(var i = 0; i < data.idx.length; i++){
            if(data.idx[i] <0 || data.idx[i] > 5){continue;}
            var card = deck.deal();
            players[socket.id].hand.cards[data.idx[i]] = card;
            socket.emit('new-card', {idx: data.idx[i], card: card});
            socket.broadcast.emit('new-hidden-card',{idx: data.idx[i]});
        }
        socket.emit('changed');
        players[socket.id].hasChanged = true;
        socket.emit('msg','Waiting for other player to change cards');
        if(allPlayersHave('hasChanged')) {
            gamePhase = 3;
            io.emit('msg','Place your bet');
            io.emit('bet');
            turnFalse('hasChanged');
        }
    });

    function reveal(){
        io.emit('show-send',{flag:false});
        var hands = [];
        for (var i = 0; i < connected.length; i++) {
            var hand = players[connected[i]].hand.cards;
            hands[i] = [];
            for(var j = 0; j < hand.length; j++){
                io.emit('reveal-card',{pid:connected[i],idx:j,card:hand[j]});
                hands[i].push(formatCard(hand[j])); // Convert cards into the format preferred by the solver
            }
            hands[i] = Hand.solve(hands[i]);
            players[connected[i]].solvedHand = hands[i];
        }
        var winner = Hand.winners([hands[0], hands[1]]);
        var winnerID = null;
        var loserID = null;
        for (var i = 0; i < connected.length; i++) {
            if(players[connected[i]].solvedHand.cards == winner[0].cards){
                //console.log(connected[i]+' wins');
                winnerID = connected[i];
            }else{
                //console.log(connected[i]+' loses');
                loserID = connected[i];
            }
        }
        wrapUp(winnerID,loserID);
    }

    function wrapUp(winnerID,loserID){
        players[winnerID].money += pot;
        var theEnd = false;
        for (var i = 0; i < connected.length; i++) {
            io.emit('update-money',{pid:connected[i],money:players[connected[i]].money});
            if(players[connected[i]].money == 0){
                theEnd = true;
            }
        }
        io.in(winnerID).emit('msg', 'You win!');
        io.in(loserID).emit('msg', 'You lose!');
        pot = 0;
        io.emit('pot',{pot:''});
        if(theEnd){
            io.in(winnerID).emit('win');
            io.in(loserID).emit('lose');
        }else{
            io.emit('over');
        }
    }

    function formatCard(card) {
        var r = card.rank;
        var s = card.suit.toLowerCase();
        if (r == 10) {
            r = 'T';
        }
        return r + s;
    }

    socket.on('disconnect',function(){
        removeValue(connected,socket.id);
        console.log(socket.id+' has left');
        delete players[socket.id];
        var nbPlayers = countPlayers();
        if(gamePhase > 0 && nbPlayers < 2) {
            gamePhase = 0;
            io.emit('end');
            connected = [];
            players = {};
        }
    });
});

function countPlayers(){
    return connected.length;
}

function allPlayersHave(property){
    var total = 0;
    var count = 0;
    for (var i = 0; i < connected.length; i++) {
        total++;
        if(players[connected[i]][property]){
            count++
        }
    }
    return total == count;
}

function turnFalse(property){
    for (var i = 0; i < connected.length; i++) {
        players[connected[i]][property] = false;
    }
}

function removeValue(array,val){
    var idx = array.indexOf(val);
    if(idx > -1) {
        array.splice(idx,1);
    }
}